class MailController < ApplicationController
	
	protect_from_forgery except: :mail_action

	def index

		failure = {'text'=> 'error'}

		# check
		if params[:token] != 'SpAZM1MI440Di1Buq5rxl1B4' || params[:user_name] != 'inouekoichi'
			render :json => failure
			return
		end

		# タイトル
		text = params[:text].split(' ')[1]
		time = Time.now.strftime("%H:%M")

		if text == 'start'
			subject = '始業連絡 井上 '+time
		elsif text == 'end'
			subject = '終業連絡 井上 '+time
		else			
			render :json => failure
			return
		end

		to = 'inouekoichi01@gmail.com'
		from = 'chikin72@gmail.com'
		UserNotifier.start_mail(to,from,subject).deliver

		success = {'text'=> subject}		
		render :json => success
	end
end
