class UserNotifier < ApplicationMailer
  default :from => 'inouekoichi01@gmail.com'

  def start_mail(to,from,subject)
    mail( :to => to,
    :from => from,
    :subject => subject )
  end	
end
